public class Square {
    public static void main(String[] args){
   
    }
    private double side;

    public Square(double side) {
        this.side = side;
    }

    public double getSide() {
        return this.side;
    }
    
    public double getArea() {
        return this.side * this.side;
    }

    public String toString() {
        return "The side of the square is " + this.side + ", the area of the square is " + getArea();
    }
}
