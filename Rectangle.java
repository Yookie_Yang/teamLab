public class Rectangle{
    private double length;
    private double width;

public Rectangle(double length, double width){
    this.length = length;
    this.width = width;
    if(this.length < 0){
        this.length =0;
    }
    if(this.width < 0){
        this.width =0;
    }
}
public double getLength(){
    return this.length;
}
public double getWidth(){
    return this.width;
}
public double getArea(){
    return (this.length * this.width);
}
public String toString(){
    String s ="";
    s+= "This rectangle has the length of "+ this.length +", the width of "+ this.width;
    return s;
}
}