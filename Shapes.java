public class Shapes{
    public static void main(String[] args) {
        Square shape = new Square(5);
        System.out.println(shape.toString());
        Rectangle r = new Rectangle(3.5,5.5);
        System.out.println(r);
        double area = r.getArea();
        System.out.println("The area is " + area);

    }
}